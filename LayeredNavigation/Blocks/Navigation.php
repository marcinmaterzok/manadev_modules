<?php
/**
 * @copyright   Copyright (c) http://www.manadev.com
 * @license     http://www.manadev.com/license  Proprietary License
 */

namespace Manadev\LayeredNavigation\Blocks;

use ITCP\CategoryKeeper\Model\KeeperFactory;
use ITCP\Checkboxes\Helper\CheckboxData;
use MageArray\MessageManagement\Helper\Data;
use Magento\Customer\Model\Session;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template;
use Manadev\Core\Helpers\LayoutHelper;
use Manadev\LayeredNavigation\Configuration;
use Manadev\LayeredNavigation\Engine;
use Manadev\LayeredNavigation\EngineFilter;
use Manadev\LayeredNavigation\UrlGenerator;

class Navigation extends Template
{
    /**
     * @var Engine
     */
    protected $engine;
    /**
     * @var UrlGenerator
     */
    protected $urlGenerator;
    /**
     * @var Configuration
     */
    protected $config;

    protected $_scripts = [];

    /**
     * By default all filters are visible
     *
     * @var bool
     */
    protected $defaultVisibility = true;

    /**
     * If filter is not listed in this array it is visible as specified in $defaultVisibility
     * property.
     *
     * @var bool[]
     */
    protected $visibility = [];

    /**
     * By default all applied filters are visible
     *
     * @var bool
     */
    protected $defaultAppliedFilterVisibility = true;

    /**
     * If applied filter is not listed in this array it is visible as specified in $defaultAppliedFilterVisibility
     * property.
     *
     * @var bool[]
     */
    protected $appliedFilterVisibility = [];

    /**
     * @var LayoutHelper
     */
    protected $layoutHelper;
    /**
     * @var Registry
     */
    protected $registry;
    /**
     * @var KeeperFactory
     */
    private $keeperFactory;
    /**
     * @var \ITCP\CategoryKeeper\Helper\Keeper
     */
    private $helperKeeper;
    /**
     * @var CheckboxData
     */
    private $checkboxData;

    /**
     * @var Session
     */
    private $session;
    /**
     * @var \Magento\Customer\Model\Customer
     */
    private $customer;
    /**
     * @var Data
     */
    private $dataHelper;
    /**
     * @var \Magento\Catalog\Model\Session
     */
    private $catalogSession;
    /**
     * @var \Bss\DynamicRows\Helper\Data
     */
    private $cmsHelper;

    public function __construct(
        \Bss\DynamicRows\Helper\Data $cmsHelper,
        \Magento\Catalog\Model\Session $catalogSession,
        Data $dataHelper,
        Session $session,
        CheckboxData $checkboxData,
        \ITCP\CategoryKeeper\Helper\Keeper $helperKeeper,
        KeeperFactory $keeperFactory,
        Template\Context $context,
        Engine $engine,
        UrlGenerator $urlGenerator,
        Configuration $config,
        LayoutHelper $layoutHelper,
        Registry $registry,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->engine = $engine;
        $this->urlGenerator = $urlGenerator;
        $this->config = $config;
        $this->layoutHelper = $layoutHelper;
        $this->registry = $registry;
        $this->keeperFactory = $keeperFactory;
        $this->helperKeeper = $helperKeeper;
        $this->checkboxData = $checkboxData;

        $this->session = $session;
        if ($this->session->isLoggedIn()) {
            $this->customer = $this->session->getCustomer();
        }
        $this->dataHelper = $dataHelper;
        $this->catalogSession = $catalogSession;
        $this->cmsHelper = $cmsHelper;
    }

    public function renderScripts()
    {
        return json_encode($this->_scripts, JSON_PRETTY_PRINT);
    }

    public function getApplyHtml()
    {
        /* @var \Manadev\LayeredNavigationAjax\Blocks\Apply $block */
        if (!($block = $this->getLayout()->getBlock('mana.layered-nav.apply'))) {
            return '';
        }

        return $block->getHtml();
    }

    public function getCurrentCategory()
    {
        return $this->registry->registry('current_category');
    }

    protected function _prepareLayout()
    {
        $this->layoutHelper->afterLayoutIsLoaded(function () {
            $this->engine->prepareFiltersToShowIn(
                $this->getData('position'),
                $this->defaultVisibility,
                $this->visibility
            );
        });

        return $this;
    }

    public function addScript($scriptName, $config = [], $target = '*')
    {
        if (!isset($this->_scripts[$target])) {
            $this->_scripts[$target] = [];
        }

        $this->_scripts[$target][$scriptName] = $config;

        return $this;
    }

    public function getScripts()
    {
        return $this->_scripts;
    }

    public function setCategoryId($category_id)
    {
        $this->engine->setCurrentCategory($category_id);
    }

    public function getProductCollectionCount()
    {
        return $this->engine->getProductCollection()->count();
    }

    public function isVisible()
    {
        $this->engine->getProductCollection()->loadFacets();
        foreach ($this->engine->getFiltersToShowIn($this->getData('position')) as $engineFilter) {
            if ($engineFilter->isVisible() && (
                    $this->isFilterVisible($engineFilter) ||
                    $this->isAppliedFilterVisibleAsSpecifiedInLayoutXml($engineFilter)
            )) {
                return true;
            }
        }

        return false;
    }

    public function hasState()
    {
        foreach ($this->getAppliedFilters() as $engineFilter) {
            return true;
        }

        return false;
    }

    public function getClearUrl()
    {
        $filtersNotToBeCleared = [];
        foreach ($this->engine->getFilters() as $engineFilter) {
            if ($engineFilter->isApplied() && !$this->isAppliedFilterVisibleAsSpecifiedInLayoutXml($engineFilter)) {
                $filtersNotToBeCleared[] = $engineFilter;
            }
        }
        return $this->escapeUrl($this->urlGenerator->getClearAllUrl($filtersNotToBeCleared));
    }

    public function getUrlWithoutParameters()
    {
        return strtok($this->getClearUrl(), '?');
    }

    public function getClearLinkAttributes()
    {
        return $this->urlGenerator->renderLastAction();
    }

    public function getRemoveFilterUrl(EngineFilter $engineFilter)
    {
        /** @var FilterRenderer $filterRenderer */
        $filterRenderer = $this->getChildBlock('filter_renderer');

        return $filterRenderer->getRemoveItemUrl($engineFilter);
    }

    public function getRemoveFilterLinkAttributes($filter)
    {
        return $this->urlGenerator->renderLastAction();
    }

    /**
     * @return EngineFilter[]
     */
    public function getFilters()
    {
        foreach ($this->engine->getFiltersToShowIn($this->getData('position')) as $engineFilter) {
            if ($engineFilter->isVisible() && $this->isFilterVisible($engineFilter)) {
                yield $engineFilter;
            }
        }
    }

    /**
     * @return EngineFilter[]
     */
    public function getAppliedFilters()
    {
        foreach ($this->engine->getFilters() as $engineFilter) {
            if ($engineFilter->isApplied() && $this->isAppliedFilterVisibleAsSpecifiedInLayoutXml($engineFilter)) {
                yield $engineFilter;
            }
        }
    }

    public function renderFilter(EngineFilter $engineFilter)
    {
        /* @var $filterRenderer FilterRenderer */
        $filterRenderer = $this->getChildBlock('filter_renderer');

        return $filterRenderer->render($engineFilter);
    }

    /**
     * @return int
     */
    public function getAppliedOptionCount()
    {
        $count = 0;
        foreach ($this->getAppliedFilters() as $engineFilter) {
            foreach ($engineFilter->getAppliedItems() as $item) {
                $count++;
            }
        }

        return $count;
    }

    public function renderAppliedItem(EngineFilter $engineFilter, $item)
    {
        /* @var $appliedItemRenderer AppliedItemRenderer */
        $appliedItemRenderer = $this->getChildBlock('applied_item_renderer');

        return $appliedItemRenderer->render($engineFilter, $item);
    }

    /**
     * @return bool
     */
    public function isAppliedFilterVisible()
    {
        return $this->config->isAppliedFilterVisible($this->getData('position'));
    }

    public function hide($name)
    {
        $this->visibility[$name] = false;
    }

    public function show($name)
    {
        $this->visibility[$name] = true;
    }

    public function hideAll()
    {
        $this->defaultVisibility = false;
    }

    public function showAll()
    {
        $this->defaultVisibility = true;
    }

    public function hideAppliedFilter($name)
    {
        $this->appliedFilterVisibility[$name] = false;
    }

    public function showAppliedFilter($name)
    {
        $this->appliedFilterVisibility[$name] = true;
    }

    public function hideAllAppliedFilters()
    {
        $this->defaultAppliedFilterVisibility = false;
    }

    public function showAllAppliedFilters()
    {
        $this->defaultAppliedFilterVisibility = true;
    }

    public function setFilterPosition($filter, $position)
    {
        $this->engine->setFilterPosition($filter, $position);
    }

    /**
     * @param EngineFilter $engineFilter
     * @return bool
     */
    protected function isFilterVisible($engineFilter)
    {
        if (isset($this->visibility[$engineFilter->getFilter()->getData('param_name')])) {
            if ($this->visibility[$engineFilter->getFilter()->getData('param_name')] === false) {
                return false;
            }
        } elseif (!$this->defaultVisibility) {
            return false;
        }

        return true;
    }

    /**
     * @param EngineFilter $engineFilter
     * @return bool
     */
    protected function isAppliedFilterVisibleAsSpecifiedInLayoutXml($engineFilter)
    {
        if (isset($this->appliedFilterVisibility[$engineFilter->getFilter()->getData('param_name')])) {
            if ($this->appliedFilterVisibility[$engineFilter->getFilter()->getData('param_name')] === false) {
                return false;
            }
        } elseif (!$this->defaultAppliedFilterVisibility) {
            return false;
        }

        return true;
    }


    public function getCategoryKeeper()
    {
        return $this->helperKeeper->loadCategoryKeeper($this->getCurrentCategory());
    }

    public function getCheckbox()
    {
        return $this->checkboxData->getBokCheckbox();
    }

    public function getCustomerFirstName()
    {
        return $this->customer ? $this->customer->getData('firstname') : '';
    }

    public function getCustomerEmail()
    {
        return $this->customer ? $this->customer->getEmail() : '';
    }

    public function getCustomerPhone()
    {
        return $this->customer ? $this->customer->getData('customer_phone') : '';
    }

    public function getAllowfileType()
    {
        return $this->dataHelper->getAllowfileType();
    }

    public function isLoggedCustomer()
    {
        return $this->session->isLoggedIn();
    }

    public function getCheckboxData()
    {
        return $this->checkboxData->getKeeperCheckbox();
    }

    public function getKeeperFormAction()
    {
        return '/keeper/index/save';
    }

    public function isSuccessModal()
    {
        return $this->catalogSession->getData('modal_success_category_keeper', true);
    }

    public function askKeeperName()
    {
        return $this->catalogSession->getData('keeper_ask_name', true);
    }

    public function getModalThanks()
    {
        $text = $this->cmsHelper->getGeneralConfig('categoryKeeper/general/', 'modal_thanks');
        return str_replace('#custmerName#',$this->askKeeperName(),$text);
    }

    public function askKeeperId()
    {
        return $this->catalogSession->getData('keeper_ask_id', true);
    }

    public function getUserData()
    {
        $customer = $this->session->getCustomer();
        return [$customer->getFirstname(), $customer->getLastname(), $customer->getEmail()];
    }
}
